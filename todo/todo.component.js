(function () {
  angular.module('todo')
    .component('todo', {
      templateUrl: 'todo/todo.template.html',
      controller: [TodoController],
      bindings: {
        date: '<',
        tasks: '<',
        onCreate: '&'
      }
    });

  function TodoController() {
    var ctrl = this;
    ctrl.create = create;
    ctrl.task = newTask();

    function create(task) {
      ctrl.tasks.push(task);
      ctrl.task = newTask();
      ctrl.onCreate({ task: task });
    }

    function newTask() {
      return {
        description: '',
        completed: false,
        date: ctrl.date || new Date()
      };
    }
  }
})();
