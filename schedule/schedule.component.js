(function () {
  angular.module('schedule')
    .component('schedule', {
      templateUrl: 'schedule/schedule.template.html',
      controller: [ScheduleController],
      bindings: {}
    });

  function ScheduleController() {
    var ctrl = this;
    ctrl.showTodo = false;
    ctrl.todoIcon = '\u2713';
    ctrl.date = new Date();
    ctrl.todos = new Map();
    ctrl.tasks = [];
    ctrl.select = select;
    ctrl.toggleTodo = toggleTodo;
    ctrl.createTask = createTask;

    function select(day) {
      ctrl.date = day.date;
      ctrl.tasks = ctrl.todos.get(getDateString(day.date)) || [];
      toggleTodo();
    }

    function toggleTodo() {
      ctrl.showTodo = !ctrl.showTodo;
      ctrl.todoIcon = ctrl.showTodo ? '\u2718' : '\u2713';
    }

    function createTask(task) {
      var dateString = getDateString(ctrl.date);
      var tasks = ctrl.todos.get(dateString) || [];
      tasks.push(task);
      ctrl.todos.set(dateString, tasks);
      console.log(ctrl.todos);
    }
  }

  function getDateString(date) {
    return date.toISOString().split('T')[0];
  }
})();
