(function () {
  angular.module('calendar')
    .component('calendar', {
      templateUrl: 'calendar/calendar.template.html',
      controller: ['dateHelper', 'calendarHelper', CalendarController],
      bindings: {
        month: '@?',
        year: '@?',
        onSelect: '&'
      }
    });

  function CalendarController(dateHelper, calendarHelper) {
    var ctrl = this;
    ctrl.$onInit = onInit;
    ctrl.nextMonth = nextMonth;
    ctrl.previousMonth = previousMonth;
    ctrl.select = select;
    ctrl.selected = calendarHelper.buildDay(new Date());

    function onInit() {
      build(dateHelper.getDateSelectedMonthYear(ctrl.month, ctrl.year));
    }

    function build(date) {
      ctrl.date = date;
      ctrl.selectedMonthName = dateHelper.getMonthName(date.getMonth());
      ctrl.selectedYear = date.getFullYear();
      ctrl.weeks = calendarHelper.buildWeeks(date, ctrl.selected.date);
    }

    function nextMonth(date) {
      var next = dateHelper.cloneDate(date);
      next.setMonth(next.getMonth() + 1);
      build(next);
    }

    function previousMonth(date) {
      var previous = dateHelper.cloneDate(date);
      previous.setMonth(previous.getMonth() - 1);
      build(previous);
    }

    function select(day) {
      unactiveDays(ctrl.weeks);
      day.activeCss = 'active';
      ctrl.selected = day;
      ctrl.onSelect({ day: day });
    }

    function unactiveDays(weeks) {
      weeks.forEach(function (week) {
        week.forEach(function (day) {
          day.activeCss = '';
        });
      });
    }
  }
})();
