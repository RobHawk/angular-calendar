(function () {
  angular.module('core')
    .factory('calendarHelper', function (dateHelper) {
      return new CalendarHelper(dateHelper);
    });

  function CalendarHelper(dateHelper) {
    var ctrl = this;
    ctrl.buildWeeks = buildWeeks;
    ctrl.buildWeek = buildWeek;
    ctrl.buildDay = buildDay;
    ctrl.getDateLastSunday = getDateLastSunday;

    function buildWeeks(date, selectedDate) {
      var weeks = [];
      var initialDate = getDateLastSunday(date);
      for (var i = 0; i < 6; i++)
        weeks.push(buildWeek(initialDate, selectedDate));
      return weeks;
    }

    function buildWeek(date, selectedDate) {
      var week = [];
      for (var i = 0; i < 7; i++) {
        week.push(buildDay(date, selectedDate));
        date.setDate(date.getDate() + 1);
      }
      return week;
    }

    function buildDay(date, selectedDate) {
      return {
        date: dateHelper.cloneDate(date),
        todayCss: dateHelper.isSameDate(date, new Date()) ? 'today' : '',
        activeCss: dateHelper.isSameDate(date, selectedDate) ? 'active' : ''
      };
    }

    function getDateLastSunday(selectedDate) {
      var date = dateHelper.cloneDate(selectedDate);
      date.setDate(1);
      var daysPastFromLastSunday = date.getDay() === 0 ? 7 : date.getDay();
      date.setDate(date.getDate() - daysPastFromLastSunday);
      return date;
    }
  }
})();
