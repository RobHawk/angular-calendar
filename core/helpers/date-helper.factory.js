(function () {
  angular.module('core')
    .factory('dateHelper', function () {
      return new DateHelper();
    });

  function DateHelper() {
    var ctrl = this;
    ctrl.isSameDate = isSameDate;
    ctrl.cloneDate = cloneDate;
    ctrl.getDateSelectedMonthYear = getDateSelectedMonthYear;
    ctrl.getMonthName = getMonthName;

    function isSameDate(date1, date2) {
      if (date1 instanceof Date && date2 instanceof Date) {
        var sameYear = date1.getFullYear() === date2.getFullYear();
        var sameMonth = date1.getMonth() === date2.getMonth();
        var sameDayOfMonth = date1.getDate() === date2.getDate();
        return sameYear && sameMonth && sameDayOfMonth;
      }
      return false;
    }

    function cloneDate(date) {
      return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    }

    function getDateSelectedMonthYear(month, year) {
      var date = new Date();
      date.setDate(1);
      if (month >= 1 && month <= 12)
        date.setMonth(--month);
      if (year >= 1800 && year <= 2300)
        date.setFullYear(year);
      return date;
    }

    function getMonthName(month) {
      switch (month) {
        case 0:
          return 'Janeiro';
        case 1:
          return 'Fevereiro';
        case 2:
          return 'Março';
        case 3:
          return 'Abril';
        case 4:
          return 'Maio';
        case 5:
          return 'Junho';
        case 6:
          return 'Julho';
        case 7:
          return 'Agosto';
        case 8:
          return 'Setembro';
        case 9:
          return 'Outubro';
        case 10:
          return 'Novembro';
        case 11:
          return 'Dezembro';
      }
    }
  }
})();
