(function () {
  angular.module('core')
    .filter('date', function () {
      return function (date) {
        var dateTime = date.toISOString().split('T');
        var ymd = dateTime[0].split('-');
        return ymd.reverse().join('/');
      };
    });
})();
